#include "../graphic/simple_graphics.h"
#include "../terasic_lib/terasic_includes.h"
#include <string.h>
#include <io.h>
#include "sys/alt_alarm.h"
#include "sys/alt_cache.h"
#include "system.h"
#include "../graphic/alt_video_display.h"
#include "../graphic/ILI9341.h"

void vid_clean_screen(alt_video_display* display, int color) {
	vid_paint_block(0, 0, display->width, display->height, color, display);
}

/******************************************************************
 *  Function: vid_draw_line
 *
 *  Purpose: Draws a line between two end points. First checks
 *           to see if the line is horizontal.  If it is, it calls
 *           vid_draw_horiz_line(), which is much faster than
 *           vid_draw_sloped_line.
 *
 ******************************************************************/
__inline__ void vid_draw_line(int horiz_start, int vert_start, int horiz_end,
		int vert_end, int width, int color, alt_video_display* display) {

	if (vert_start == vert_end) {

		vid_draw_horiz_line((unsigned short) horiz_start,
				(unsigned short) horiz_end, (unsigned short) vert_start, color,
				display);
	} else {
		vid_draw_sloped_line((unsigned short) horiz_start,
				(unsigned short) vert_start, (unsigned short) horiz_end,
				(unsigned short) vert_end, (unsigned short) width, color,
				display);
	}
}

/******************************************************************
 *  Function: vid_draw_box
 *
 *  Purpose: Draws a box on the screen with the specified corner
 *  points.  The fill parameter tells the function whether or not
 *  to fill in the box.  1 = fill, 0 = do not fill.
 *
 ******************************************************************/
int vid_draw_box(int horiz_start, int vert_start, int horiz_end, int vert_end,
		int color, int fill, alt_video_display* display) {

	// If we want to fill in our box
	if (fill) {
		vid_paint_block(horiz_start, vert_start, horiz_end, vert_end, color,
				display);
		// If we're not filling in the box, just draw four lines.
	} else {
		vid_draw_line(horiz_start, vert_start, horiz_start, vert_end - 1, 1,
				color, display);
		vid_draw_line(horiz_end - 1, vert_start, horiz_end - 1, vert_end - 1, 1,
				color, display);
		vid_draw_line(horiz_start, vert_start, horiz_end - 1, vert_start, 1,
				color, display);
		vid_draw_line(horiz_start, vert_end - 1, horiz_end - 1, vert_end - 1, 1,
				color, display);
	}

	return (0);
}

int vid_draw_table(int horiz_start, int vert_start, int horiz_end, int vert_end,
		int num_x_row, int num_y_col, int color, alt_video_display* display) {
	int pos_x_deviation = (vert_end - vert_start) / num_x_row;
	int pos_y_deviation = (horiz_end - horiz_start) / num_y_col;

	//draw box
	vid_draw_box(horiz_start, vert_start, horiz_end, vert_end, color, 1,
			display);

	//draw x lines
	int y = vert_start + pos_x_deviation;
	for (int i = 0; i < num_x_row - 1; i++) {
		vid_draw_line(horiz_start, y, horiz_end - 1, y, 1, BLACK_16, display);
		y += pos_x_deviation;
	}

	//draw y lines
	int x = horiz_start + pos_y_deviation;
	for (int i = 0; i < num_y_col - 1; i++) {
		vid_draw_line(x, vert_start, x, vert_end - 1, 1, GREEN_16, display);
		x += pos_y_deviation;
	}

	return (0);
}

/******************************************************************
 *  Function: vid_set_pixel
 *
 *  Purpose: Sets the specified pixel to the specified color.
 *           Sets one pixel although frame buffer consists of
 *           two-pixel words.  Therefore this function is not
 *           efficient when painting large areas of the screen.
 *
 ******************************************************************/

void vid_set_pixel(int horiz, int vert, unsigned int color,
		alt_video_display* display) {

	alt_u16 color16;

	// encode to RGB  5 6 5
	color16 = (color & 0xFF) >> 3; // blue
	color16 |= (color & 0xFC00) >> 5; // green
	color16 |= (color & 0xF80000) >> 8; // blue
	LCD_DrawPoint(horiz, vert, color16);
}

/******************************************************************
 *  Function: vid_paint_block
 *
 *  Purpose: Paints a block of the screen the specified color.
 *           Note: works with two pixels at a time to maintain high
 *           bandwidth.  Therefore, corner points must be even
 *           numbered coordinates.  Use vid_draw_solid_box() for
 *           areas with odd-numbered corner points.
 *           The color parameter must contain two pixel's worth
 *           (32 bits).
 *
 ******************************************************************/
void vid_paint_block(int Hstart, int Vstart, int Hend, int Vend, int color,
		alt_video_display* display) {

	int x, y;
	for (y = Vstart; y < Vend; y++) {
		for (x = Hstart; x < Hend; x++) {
			vid_set_pixel(x, y, color, display);
		}
	}
}

/******************************************************************
 *  Function: vid_draw_horiz_line
 *
 *  Purpose: Draws a horizontal line on the screen quickly.
 *           Good for filling stuff.
 *
 ******************************************************************/
void vid_draw_horiz_line(short Hstart, short Hend, int V, int color,
		alt_video_display* display) {
	int x;
	for (x = Hstart; x < Hend; x++) {
		vid_set_pixel(x, V, color, display);
	}

}

/******************************************************************
 *  Function: vid_draw_sloped_line
 *
 *  Purpose: Draws a line between two end points using
 *           Bresenham's line drawing algorithm.
 *           width parameter is not used.
 *           It is reserved for future use.
 *
 ******************************************************************/
void vid_draw_sloped_line(unsigned short horiz_start, unsigned short vert_start,
		unsigned short horiz_end, unsigned short vert_end, unsigned short width,
		int color, alt_video_display* display) {
	// Find the vertical and horizontal distance between the two points
	int horiz_delta = abs(horiz_end - horiz_start);
	int vert_delta = abs(vert_end - vert_start);

	// Find out what direction we are going
	int horiz_incr, vert_incr;
	if (horiz_start > horiz_end) {
		horiz_incr = -1;
	} else {
		horiz_incr = 1;
	}
	if (vert_start > vert_end) {
		vert_incr = -1;
	} else {
		vert_incr = 1;
	}

	// Find out which axis is always incremented when drawing the line
	// If it's the horizontal axis
	if (horiz_delta >= vert_delta) {
		int dPr = vert_delta << 1;
		int dPru = dPr - (horiz_delta << 1);
		int P = dPr - horiz_delta;

		// Process the line, one horizontal point at at time
		for (; horiz_delta >= 0; horiz_delta--) {
			// plot the pixel
			vid_set_pixel(horiz_start, vert_start, color, display);
			// If we're moving both up and right
			if (P > 0) {
				horiz_start += horiz_incr;
				vert_start += vert_incr;
				P += dPru;
			} else {
				horiz_start += horiz_incr;
				P += dPr;
			}
		}
		// If it's the vertical axis
	} else {
		int dPr = horiz_delta << 1;
		int dPru = dPr - (vert_delta << 1);
		int P = dPr - vert_delta;

		// Process the line, one vertical point at at time
		for (; vert_delta >= 0; vert_delta--) {
			// plot the pixel
			vid_set_pixel(horiz_start, vert_start, color, display);
			// If we're moving both up and right
			if (P > 0) {
				horiz_start += horiz_incr;
				vert_start += vert_incr;
				P += dPru;
			} else {
				vert_start += vert_incr;
				P += dPr;
			}
		}
	}
}

/******************************************************************
 *  Function: vid_draw_circle
 *
 *  Purpose: Draws a circle on the screen with the specified center
 *  and radius.  Draws symetric circles only.  The fill parameter
 *  tells the function whether or not to fill in the box.  1 = fill,
 *  0 = do not fill.
 *
 ******************************************************************/
int vid_draw_circle(int Hcenter, int Vcenter, int radius, int color, char fill,
		alt_video_display* display) {
	int x = 0;
	int y = radius;
	int p = (5 - radius * 4) / 4;

	// Start the circle with the top, bottom, left, and right pixels.
	vid_round_corner_points(Hcenter, Vcenter, x, y, 0, 0, color, fill, display);

	// Now start moving out from those points until the lines meet
	while (x < y) {
		x++;
		if (p < 0) {
			p += 2 * x + 1;
		} else {
			y--;
			p += 2 * (x - y) + 1;
		}
		vid_round_corner_points(Hcenter, Vcenter, x, y, 0, 0, color, fill,
				display);
	}
	return (0);
}

/******************************************************************
 *  Function: vid_draw_round_corner_box
 *
 *  Purpose: Draws a box on the screen with the specified corner
 *  points.  The fill parameter tells the function whether or not
 *  to fill in the box.  1 = fill, 0 = do not fill.
 *
 ******************************************************************/
int vid_draw_round_corner_box(int horiz_start, int vert_start, int horiz_end,
		int vert_end, int radius, int color, int fill,
		alt_video_display* display) {
	unsigned int x, y;
	int p;
	int diameter;
	int temp;
	unsigned int width, height, straight_width, straight_height;

	// Make sure the start point us up and left of the end point
	if (horiz_start > horiz_end) {
		temp = horiz_end;
		horiz_end = horiz_start;
		horiz_start = temp;
	}

	if (vert_start > vert_end) {
		temp = vert_end;
		vert_end = vert_start;
		vert_start = temp;
	}

	// These are the overall dimensions of the box
	width = horiz_end - horiz_start;
	height = vert_end - vert_start;

	// Make sure our radius isnt more than the shortest dimension
	// of the box, or it'll screw us all up
	if (radius > (width / 2))
		radius = width / 2;

	if (radius > (height / 2))
		radius = height / 2;

	// We use the diameter for some calculations, so we'll pre calculate it here.
	diameter = (radius * 2);

	// These are the lengths of the straight portions of the box edges.
	straight_width = width - diameter;
	straight_height = height - diameter;

	x = 0;
	y = radius;
	p = (5 - radius * 4) / 4;

	// Start the corners with the top, bottom, left, and right pixels.
	vid_round_corner_points(horiz_start + radius, vert_start + radius, x, y,
			straight_width, straight_height, color, fill, display);

	// Now start moving out from those points until the lines meet
	while (x < y) {
		x++;
		if (p < 0) {
			p += 2 * x + 1;
		} else {
			y--;
			p += 2 * (x - y) + 1;
		}
		vid_round_corner_points(horiz_start + radius, vert_start + radius, x, y,
				straight_width, straight_height, color, fill, display);
	}

	// If we want to fill in our box
	if (fill) {
		vid_paint_block(horiz_start, vert_start + radius, horiz_end,
				vert_end - radius, color, display);
		// If we're not filling in the box, just draw four lines.
	} else {
		vid_draw_line(horiz_start, vert_start + radius, horiz_start,
				vert_end - radius, 1, color, display); //left
		vid_draw_line(horiz_end, vert_start + radius, horiz_end,
				vert_end - radius, 1, color, display); //right
		vid_draw_line(horiz_start + radius, vert_start, horiz_end - radius,
				vert_start, 1, color, display); //top
		vid_draw_line(horiz_start + radius, vert_end, horiz_end - radius,
				vert_end, 1, color, display); //bottom
	}

	return (0);
}

/******************************************************************
 *  Function: vid_round_corner_points
 *
 *  Purpose: Called by vid_draw_round_corner_box() and
 *  vid_draw_circle() to plot the actual points of the round corners.
 *  Draws horizontal lines to fill the shape.
 *
 ******************************************************************/

void vid_round_corner_points(int cx, int cy, int x, int y, int straight_width,
		int straight_height, int color, char fill, alt_video_display* display) {

	// If we're directly above, below, left and right of center (0 degrees), plot those 4 pixels
	if (x == 0) {
		// bottom
		vid_set_pixel(cx, cy + y + straight_height, color, display);
		vid_set_pixel(cx + straight_width, cy + y + straight_height, color,
				display);
		// top
		vid_set_pixel(cx, cy - y, color, display);
		vid_set_pixel(cx + straight_width, cy - y, color, display);

		if (fill) {
			vid_draw_line(cx - y, cy, cx + y + straight_width, cy, 1, color,
					display);
			vid_draw_line(cx - y, cy + straight_height, cx + y + straight_width,
					cy + straight_height, 1, color, display);
		} else {
			//right
			vid_set_pixel(cx + y + straight_width, cy, color, display);
			vid_set_pixel(cx + y + straight_width, cy + straight_height, color,
					display);
			//left
			vid_set_pixel(cx - y, cy, color, display);
			vid_set_pixel(cx - y, cy + straight_height, color, display);
		}

	} else
	// If we've reached the 45 degree points (x=y), plot those 4 pixels
	if (x == y) {
		if (fill) {
			vid_draw_line(cx - x, cy + y + straight_height,
					cx + x + straight_width, cy + y + straight_height, 1, color,
					display); // lower
			vid_draw_line(cx - x, cy - y, cx + x + straight_width, cy - y, 1,
					color, display); // upper

		} else {
			vid_set_pixel(cx + x + straight_width, cy + y + straight_height,
					color, display); // bottom right
			vid_set_pixel(cx - x, cy + y + straight_height, color, display); // bottom left
			vid_set_pixel(cx + x + straight_width, cy - y, color, display); // top right
			vid_set_pixel(cx - x, cy - y, color, display); // top left
		}
	} else
	// If we're between 0 and 45 degrees plot 8 pixels.
	if (x < y) {
		if (fill) {
			vid_draw_line(cx - x, cy + y + straight_height,
					cx + x + straight_width, cy + y + straight_height, 1, color,
					display);
			vid_draw_line(cx - y, cy + x + straight_height,
					cx + y + straight_width, cy + x + straight_height, 1, color,
					display);
			vid_draw_line(cx - y, cy - x, cx + y + straight_width, cy - x, 1,
					color, display);
			vid_draw_line(cx - x, cy - y, cx + x + straight_width, cy - y, 1,
					color, display);
		} else {
			vid_set_pixel(cx + x + straight_width, cy + y + straight_height,
					color, display);
			vid_set_pixel(cx - x, cy + y + straight_height, color, display);
			vid_set_pixel(cx + x + straight_width, cy - y, color, display);
			vid_set_pixel(cx - x, cy - y, color, display);
			vid_set_pixel(cx + y + straight_width, cy + x + straight_height,
					color, display);
			vid_set_pixel(cx - y, cy + x + straight_height, color, display);
			vid_set_pixel(cx + y + straight_width, cy - x, color, display);
			vid_set_pixel(cx - y, cy - x, color, display);
		}
	}
}

/******************************************************************
 *  Function: min3
 *
 *  Purpose:  Returns the minimum value of 3 parameters
 *            Used for drawing filled shapes
 *
 ******************************************************************/
int max3(int a, int b, int c) {
	if (a < b)
		a = b;
	if (a < c)
		a = c;

	return a;
}

/******************************************************************
 *  Function: min3
 *
 *  Purpose:  Returns the minimum value of 3 parameters
 *            Used for drawing filled shapes.
 *
 ******************************************************************/
int min3(int a, int b, int c) {
	if (a > b)
		a = b;
	if (a > c)
		a = c;

	return a;
}

/******************************************************************
 *  Function: max_diff3
 *
 *  Purpose:  Returns the positive max difference between 3
 *            parameters.  Used for drawing filled shapes
 *
 ******************************************************************/
__inline__ int max_diff3(int a, int b, int c) {
	int max, min;

	max = max3(a, b, c);
	min = min3(a, b, c);
	return (max - min);
}

/******************************************************************
 *  Function: vid_put_pixel_in_span_map
 *
 *  Purpose:  This function places a pixel into either the max or
 *            min value of an element that represents a span,
 *            essentially just a horizontal line used to fill shapes.
 *
 ******************************************************************/
void vid_put_pixel_in_span_map(int x, int y, int *span_array) {
	if (span_array[y * 2] == -1) {
		span_array[y * 2] = x;
		span_array[(y * 2) + 1] = x;
	} else if (span_array[y * 2] > x)
		span_array[y * 2] = x;
	else if (span_array[(y * 2) + 1] < x)
		span_array[(y * 2) + 1] = x;
}

/******************************************************************
 *  Function: vid_bres_scan_edges
 *
 *  Purpose:  This function uses Bresenham's algorithm to scan a line
 *            and determine whether it's the left(min) or right(max)
 *            edge of a horizontal span.  This is useful for drawing
 *            filled shapes where you fill by drawing successive
 *            horizontal lines.
 *
 ******************************************************************/
void vid_bres_scan_edges(int x1, int y1, int x2, int y2, int *span_array) {

	int x_incr, y_incr;
	int y_delta, x_delta;

	// Assure we always draw left to right
	if (x1 > x2) {
		int tempx = x2;
		x2 = x1;
		x1 = tempx;
		int tempy = y2;
		y2 = y1;
		y1 = tempy;
	}

	// Find the vertical and horizontal distance between the two points
	y_delta = abs(y1 - y2);
	x_delta = (x2 - x1);

	// Find out what direction we are going
	if (y1 > y2) {
		y_incr = -1;
	} else {
		y_incr = 1;
	}
	x_incr = 1;

	// Find out which axis is always incremented when drawing the line
	// If it's the horizontal axis
	if (x_delta >= y_delta) {
		int dPr = y_delta << 1;
		int dPru = dPr - (x_delta << 1);
		int P = dPr - x_delta;

		// Process the line, one horizontal point at at time
		for (; x_delta >= 0; x_delta--) {
			// map the pixel
			vid_put_pixel_in_span_map(x1, y1, span_array);
			// If we're moving along both axis
			if (P > 0) {
				x1 += x_incr;
				y1 += y_incr;
				P += dPru;
			} else {
				x1 += x_incr;
				P += dPr;
			}
		}
	} else // If it's the vertical axis
	{
		int dPr = x_delta << 1;
		int dPru = dPr - (y_delta << 1);
		int P = dPr - y_delta;

		// Process the line, one vertical point at at time
		for (; y_delta >= 0; y_delta--) {
			// plot the pixel
			vid_put_pixel_in_span_map(x1, y1, span_array);
			// If we're moving along both axis
			if (P > 0) {
				x1 += x_incr;
				y1 += y_incr;
				P += dPru;
			} else {
				y1 += y_incr;
				P += dPr;
			}
		}
	}
}

/******************************************************************
 *  Function: vid_draw_triangle
 *
 *  Purpose:  This function draws a triangle on the screen between
 *            three points defined by the structure tri.
 *
 ******************************************************************/
void vid_draw_triangle(triangle_struct* tri, alt_video_display* display) {
	int i;

	// Outline it first
	vid_draw_line(tri->vertex_x[0], tri->vertex_y[0], tri->vertex_x[1],
			tri->vertex_y[1], 1, tri->col, display);
	vid_draw_line(tri->vertex_x[1], tri->vertex_y[1], tri->vertex_x[2],
			tri->vertex_y[2], 1, tri->col, display);
	vid_draw_line(tri->vertex_x[2], tri->vertex_y[2], tri->vertex_x[0],
			tri->vertex_y[0], 1, tri->col, display);

	if (tri->fill == DO_FILL) {
		tri->top_y = min3(tri->vertex_y[0], tri->vertex_y[1], tri->vertex_y[2]);
		tri->bottom_y = max3(tri->vertex_y[0], tri->vertex_y[1],
				tri->vertex_y[2]);
		tri->spans_needed = max_diff3(tri->vertex_y[0], tri->vertex_y[1],
				tri->vertex_y[2]);
		tri->max_span = max_diff3(tri->vertex_x[0], tri->vertex_x[1],
				tri->vertex_x[2]);
		tri->span_array = malloc(display->height * 4 * 2);

		//init the span array
		for (i = tri->top_y; i <= tri->bottom_y; i++) {
			tri->span_array[i * 2] = -1;
			tri->span_array[(i * 2) + 1] = -1;
		}

		// Scan-convert the triangle
		vid_bres_scan_edges(tri->vertex_x[0], tri->vertex_y[0],
				tri->vertex_x[1], tri->vertex_y[1], tri->span_array);
		vid_bres_scan_edges(tri->vertex_x[1], tri->vertex_y[1],
				tri->vertex_x[2], tri->vertex_y[2], tri->span_array);
		vid_bres_scan_edges(tri->vertex_x[2], tri->vertex_y[2],
				tri->vertex_x[0], tri->vertex_y[0], tri->span_array);

		// Render the polygon
		for (i = tri->top_y; i <= tri->bottom_y; i++) {
			vid_draw_line(tri->span_array[i * 2], i,
					tri->span_array[(i * 2) + 1], i, 1, tri->col, display);

		}
		free(tri->span_array);
	}
}

