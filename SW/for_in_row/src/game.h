/*
 * game.h
 *
 */

#ifndef SRC_GAME_H_
#define SRC_GAME_H_

#define X_DIM 7
#define Y_DIM 6

typedef int bool;
#define TRUE    1
#define FALSE   0
#define MAX_GAMES 3

typedef struct{
	int P1;
	int P2;
	int board[X_DIM][Y_DIM];
	int result;
	bool finish;
}game;

void initGame(game* g);
bool userChoice(int user,int x,int y,game *g);
void checkWin(game* g);
bool checkValidity(int x, int y, game *g);

#endif /* SRC_GAME_H_ */
