#include "../terasic_lib/terasic_includes.h"
#include "gui.h"

#include "../graphic/alt_video_display.h"
#include "../graphic/simple_graphics.h"
#include "../terasic_lib/touch_spi.h"
#include "geometry.h"
#include "game.h"
#include "system.h"

#define EDGE_CAPTURE_REG_FFSET		3

#define NONE_PRESSED 0x3  // Value read from KEY PIO when no buttons pressed
#define DEBOUNCE 30000  // Time in microseconds to wait for switch debounce

void restartplay(int buttons, alt_video_display *pDisplay, DESK_INFO *DeskInfo,
		game *g);
void GUI_Draw_Battlefield(alt_video_display *pDisplay, DESK_INFO *pDeskInfo);
bool finishGame(game *g);

//RICORDA X LATO CORTO, Y LATO LUNGO

void init(alt_video_display *pDisplay, DESK_INFO *pDeskInfo, game *g) {

	// clean screen
	vid_clean_screen(pDisplay, BLACK_24);

	//Area
	RectSet(&pDeskInfo->rcPaint, RIGHT_SPACE, pDisplay->height - LEFT_SPACE,
			pDisplay->width - TOP_SPACE, BOTTOM_SPACE);

	//draw battlefield
	GUI_Draw_Battlefield(pDisplay, pDeskInfo);

	initGame(g);

}

int findEmptySquare(DESK_INFO *pDeskInfo, int pos) {
	int start_square_pos = (pos * Y_DIM) - Y_DIM;

	for (int i = start_square_pos; i < start_square_pos + 6; i++) {
		if (pDeskInfo->rcPaint.squares[i].set == FALSE) {
			return i;
		}
	}
	return -1;
}

void GUI_ColorSquare(alt_video_display *pDisplay, DESK_INFO *pDeskInfo, int pos,
		int player) {
	int color;
	if (player == 1) {
		color = BLUE_16;
	} else if (player == 2) {
		color = GREEN_16;
	}

	vid_draw_circle(pDeskInfo->rcPaint.squares[pos].y_center,
			pDeskInfo->rcPaint.squares[pos].x_center, DOT_SIZE, color,
			DO_FILL, pDisplay);
	pDeskInfo->rcPaint.squares[pos].set = TRUE;
}

int FindTriangle(POINT *pt, DESK_INFO *di) {
	triangle_struct tr;
	for (int i = 0; i < X_DIM; i++) {
		tr = di->triangles[i];
		if (CheckTriangleBorder(pt, &tr)) {
			return i + 1;
		}
	}
	return -1;
}

void GUI(alt_video_display *pDisplay, TOUCH_HANDLE *pTouch, game *g) {
	DESK_INFO DeskInfo;
	int X, Y;
	int x_pos, y_pos;
	POINT Pt;
	const int nDotSize = DOT_SIZE;
	RECT rcTouch;
	int player = 1;
	int buttons;

	//init
	init(pDisplay, &DeskInfo, g);

	rcTouch = DeskInfo.rcPaint;
	RectInflate(&rcTouch, -nDotSize - 2, -nDotSize - 2);

	while (1) {


		buttons = IORD_ALTERA_AVALON_PIO_DATA(PIO_KEY_BASE);

		// touch
		if (Touch_GetXY(pTouch, &Y, &X)) {
			printf("x=%d, y=%d\r\n", X, Y);
			PtSet(&Pt, X, Y);

			int pos = FindTriangle(&Pt, &DeskInfo);
			if (pos != -1) {
				x_pos = pos - 1;
				y_pos = findEmptySquare(&DeskInfo, pos) - (x_pos * Y_DIM);
			} else {
				continue;
			}

			if (checkValidity(x_pos, y_pos, g)) {
				if (userChoice(player, x_pos, y_pos, g) == TRUE) {
					GUI_ColorSquare(pDisplay, &DeskInfo,
							((pos - 1) * Y_DIM) + y_pos, player);
				} else {
					continue;
				}
			} else {
				continue;
			}

			if (finishGame(g) == TRUE) {
				return;
			} else {
				if (player == 1)
					player = 2;
				else
					player = 1;
			}
			//clean battlefield and restart
		} else if (buttons != NONE_PRESSED) {
			restartplay(buttons, pDisplay, &DeskInfo, g);
			player = 1;
		}
	}
}

void restartplay(int buttons, alt_video_display *pDisplay, DESK_INFO *DeskInfo,
		game *g) {
	int button_val = 0xf;  // Variable to set select switch statement value
	usleep(DEBOUNCE);
	while (buttons != NONE_PRESSED) {  // wait for button release
		button_val = buttons;   // strobe state of SW switch
		//  Sample "buttons" again to update with new value  ***
		buttons = IORD_ALTERA_AVALON_PIO_DATA(PIO_KEY_BASE);
		if (button_val == 2) {
			init(pDisplay, DeskInfo, g);
		}
	}
}

bool finishGame(game *g) {
	checkWin(g);
	if (g->finish == TRUE) {
		if (g->result == 1) {
			g->P1++;
		} else if (g->result == 2) {
			g->P2++;
		}
		return TRUE;
	}

	return FALSE;
}

void GUI_Draw_Battlefield(alt_video_display *pDisplay, DESK_INFO *pDeskInfo) {
	RECT rc;

	RectCopy(&rc, &pDeskInfo->rcPaint);
	show_table(pDisplay, rc.left, rc.bottom, rc.right, rc.top, WHITE_24);

	GUI_Draw_Arrows(pDisplay, pDeskInfo);
}

void GUI_Draw_Arrows(alt_video_display *pDisplay, DESK_INFO *pDeskInfo) {

	triangle_struct triangle;

	int x_start = pDeskInfo->rcPaint.squares[0].x_center;
	int y_start = pDisplay->width - 30;

	int deviation = pDeskInfo->rcPaint.squares[0].right
			- pDeskInfo->rcPaint.squares[0].left;

	for (int i = 0; i < NUM_COL; i++) {
		TriangleSet(NUM_COL, x_start, y_start, &triangle);
		pDeskInfo->triangles[i] = triangle;

		vid_draw_triangle(&triangle, pDisplay);
		x_start += deviation;
	}
}

void show_table(alt_video_display *pDisplay, int xstart, int ystart, int xend,
		int yend, int color) {
	printf("x1=%d,y1=%d,x2=%d,y2=%d", xstart, ystart, xend, yend);

//per me x � lato lungo, ma � lato corto
	vid_draw_table(ystart, xstart, yend, xend, 7, 6, color, pDisplay);
}
