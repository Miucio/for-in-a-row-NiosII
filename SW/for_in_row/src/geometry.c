#include "../terasic_lib/terasic_includes.h"
#include "geometry.h"
#include "../graphic/simple_graphics.h"
#include "gui.h"

bool IsPtInRect(POINT *pt, RECT *rc){
    bool bYes = FALSE;
    
    if (pt->x >= rc->left && pt->x <= rc->right && pt->y >= rc->top && pt->y <= rc->bottom)
        bYes = TRUE;
    
    return bYes;
}

int FindSquare(POINT *pt,RECT *rc){
	SQUARE sq;

	for(int i=0;i<42;i++){
		sq = rc->squares[i];
		if(pt->x >= sq.left && pt->x <= sq.right && pt->y >= sq.top && pt->y <= sq.bottom){
			return i+1;
		}
	}
	return 0;
}

bool CheckTriangleBorder(POINT *pt, triangle_struct *tr){
	int padding = 10;
	if(pt->x+padding >= tr->vertex_y[1] && pt->x <= tr->vertex_y[2]+padding && pt->y <= tr->vertex_x[2]+padding && pt->y+padding >= tr->vertex_x[0]){
		return TRUE;

	}
	return FALSE;
}

void PtSet(POINT *pt, int x, int y){
    pt->x = x;
    pt->y = y;
}

void RectSet(RECT *rc, int left, int right, int top, int bottom){
	int x_dim = (right-left)/7;
	int y_dim = (top-bottom)/6;

    rc->left = left;
    rc->right = right;
    rc->top = top;
    rc->bottom = bottom;

    int x,x1,y,y1;

    for(int i=0,pos=0;i<7;i++){
    	SQUARE sq;
    	for(int j=0;j<6;j++){
    		x=left+(i*x_dim);
    		x1=left+((i+1)*x_dim);
    		y=bottom+(j*y_dim);
    		y1=bottom+((j+1)*y_dim);
    		SquareSet(&sq,x,x1,y,y1);
    		rc->squares[pos] = sq;
    		pos++;
    	}
    }
}

void TriangleSet(int n_col,int x_start,int y_start, triangle_struct *triangle){
	int color = 0xf800;
	int height = 15;
	int width = 15;

	//vx invertito con vy
	int x1 = y_start;
	int y1 = x_start;

	triangle->vertex_x[0] = x1;
	triangle->vertex_x[1] = x1 + height;
	triangle->vertex_x[2] = x1 + height;
	triangle->vertex_y[0] = y1;
	triangle->vertex_y[1] = y1 - width;
	triangle->vertex_y[2] = y1 + width;
	triangle->col = color;
	triangle->fill = 1;
}

void SquareSet(SQUARE *sq,int left, int right, int top, int bottom){
	sq->left = left;
	sq->right = right;
	sq->top = top;
	sq->bottom = bottom;
	sq->x_center = left+((right-left)/2);
	sq->y_center = top+((bottom-top)/2);
	sq->set = FALSE;
}

void RectInflate(RECT *rc, int x, int y){
    rc->left -= x;
    rc->right += x;
    rc->top -= y;
    rc->bottom += y;
}

void RectOffset(RECT *rc, int x, int y){
    rc->left += x;
    rc->right += x;
    rc->top += y;
    rc->bottom += y;
}

void RectCopy(RECT *rcDes, RECT *rcSrc){
    rcDes->left = rcSrc->left;
    rcDes->right = rcSrc->right;
    rcDes->top = rcSrc->top;
    rcDes->bottom = rcSrc->bottom;
    //rcDes->squares = rcSrc->squares;
}

int RectWidth(RECT *rc){
    return (rc->right-rc->left);
}

int RectHeight(RECT *rc){
    return (rc->bottom-rc->top);
}

int PtDistance(POINT *pt1, POINT *pt2){
    int nDistance;
    int a, b;
    a = pt1->x - pt2->x;
    b = pt1->y - pt2->y;
    nDistance = a*a + b*b;
    nDistance = sqrt(nDistance);
    
    return nDistance;
}

void PtCopy(POINT *ptDes, POINT *ptSrc){
    ptDes->x = ptSrc->x;
    ptDes->y = ptSrc->y;
}

