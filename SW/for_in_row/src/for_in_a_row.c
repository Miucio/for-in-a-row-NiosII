/*
 * display_7.c
 *
 *  Created on: Mar 25, 2021
 *      Author: Miucio
 */

#include <stdint.h>
#include "system.h"
#include "../graphic/ILI9341.h"
#include "gui.h"
#include "game.h"

#define SEG7 		(* (volatile uint32_t*) (SEG7_BASE))
#define SIGN_7SEG  	0x40
#define P 			0x73

#define  SLIDER_1REG(value)  IOWR(SEG7_BASE,0x00,value)
#define  SLIDER_2REG(value)  IOWR(SEG7_BASE,0x01,value)
#define  SLIDER_3REG(value)  IOWR(SEG7_BASE,0x02,value)
#define  SLIDER_4REG(value)  IOWR(SEG7_BASE,0x03,value)
#define  SLIDER_5REG(value)  IOWR(SEG7_BASE,0x04,value)
#define  SLIDER_6REG(value)  IOWR(SEG7_BASE,0x05,value)

void initDisplay(alt_video_display *display);
void initSeg7();
void changeScore(game *g);
void cleanGame(game *g);
bool checkWinGame(game *g);

uint8_t bin_to_7seg(uint8_t bin) {
	const uint8_t lut[16] = { 0x3F, 0x06, 0x5B, 0x4F, // 0-3
			0x66, 0x6D, 0x7C, 0x07, // 4-7
			0x7F, 0x6F, 0x77, 0x7C, // 8-11
			0x39, 0x5E, 0x79, 0x71  // 12-15
			};
	return lut[bin & 0xF]; //&0xF per prendere solo i 4 bit meno significativi
}

int main() {
	TOUCH_HANDLE *pTouch;
	alt_video_display display;
	game game;

	// init
	pTouch = Touch_Init(TOUCH_PANEL_SPI_BASE, TOUCH_PANEL_PEN_IRQ_N_BASE,
			TOUCH_PANEL_PEN_IRQ_N_IRQ);
	if (!pTouch) {
		printf("Failed to init touch\r\n");
	} else {
		printf("Init touch successfully\r\n");

	}
	// init
	initDisplay(&display);
	initSeg7();

//	initGame(&game);

	while (1) {
		cleanGame(&game);
		//play game for MAX_GAMES try
		for (int i = 0; i < MAX_GAMES; i++) {
			initGame(&game);
			GUI(&display, pTouch, &game);
			changeScore(&game);
			if(checkWinGame(&game)) break;
		}
	}

}

bool checkWinGame(game *g){
	if(g->P1 > (MAX_GAMES/2) || g->P2 > (MAX_GAMES/2)){
		return TRUE;
	}
	return FALSE;
}

void cleanGame(game *g) {
	g->P1 = 0;
	g->P2 = 0;
	g->finish = FALSE;
	g->result = -1;
	changeScore(g);
}

void initDisplay(alt_video_display *display) {
	LCD_Init();
	LCD_Clear(0X0000);
	display->interlace = 0;
	display->bytes_per_pixel = 2;
	display->color_depth = 16;
	display->height = SCREEN_HEIGHT;
	display->width = SCREEN_WIDTH;
}

//init 7 seg
void initSeg7() {
	SLIDER_6REG(P);
	SLIDER_5REG(bin_to_7seg(1));
	SLIDER_4REG(bin_to_7seg(0));
	SLIDER_3REG(bin_to_7seg(0));
	SLIDER_2REG(P);
	SLIDER_1REG(bin_to_7seg(2));

}

void changeScore(game *g) {
	SLIDER_4REG(bin_to_7seg(g->P1));
	SLIDER_3REG(bin_to_7seg(g->P2));
}

