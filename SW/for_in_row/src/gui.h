#ifndef GUI_H_
#define GUI_H_

#include "../terasic_lib/touch_spi.h"
#include "../terasic_lib/terasic_includes.h"
#include "gui.h"

#include "../graphic/alt_video_display.h"
#include "../graphic/simple_graphics.h"
#include "../graphic/simple_graphics.h"
#include "geometry.h"
#include "game.h"

#define DOT_SIZE    	10
#define NUM_COL			7
#define NUM_ROW			6
#define RIGHT_SPACE 	41
#define LEFT_SPACE 		41
#define TOP_SPACE 		36
#define BOTTOM_SPACE	0

typedef struct{
    int Paint_Index;
    RECT rcPaint;
    triangle_struct triangles[NUM_COL];
}DESK_INFO;

int findEmptySquare(DESK_INFO *pDeskInfo, int pos);
void GUI(alt_video_display *pDisplay, TOUCH_HANDLE *pTouch, game *g);
void show_table(alt_video_display *pDisplay, int xstart, int xend, int ystart, int yend,int color);
void GUI_Draw_Arrows(alt_video_display *pDisplay, DESK_INFO *pDeskInfo);

#endif /*GUI_H_*/
