/*
 * game.c
 *
 */
#include <stdint.h>
#include "game.h"

bool checkValidity(int x, int y, game *g){
	if(x < 0 || x > X_DIM || y < 0 || y > Y_DIM){
		return FALSE;
	}
	if(g->board[x][y] == 0){
		return TRUE;
	}
	return FALSE;
}

bool userChoice(int user, int x, int y, game *g) {
	if (user == 1 && checkValidity(x, y, g)) {
		g->board[x][y] = 1;
	} else if (user == 2 && checkValidity(x, y, g)) {
		g->board[x][y] = 2;
	} else {
		printf("not valid");
		return FALSE;
	}
	return TRUE;
}

void initGame(game* g) {
	g->finish = FALSE;
	g->result = -1;
	for (int i = 0; i < X_DIM; i++) {
		for (int j = 0; j < Y_DIM; j++) {
			g->board[i][j] = 0;
		}
	}
}

void checkWin(game* g) {
	for (int i = 0; i < X_DIM; i++) {
		for (int k = 0; k < Y_DIM; k++) {
			if (g->board[i][k] != 0) {
				if (g->board[i][k] == g->board[i - 1][k - 1]
						&& g->board[i][k] == g->board[i - 2][k - 2]
						&& g->board[i][k] == g->board[i - 3][k - 3]) {
					g->finish = TRUE;
					g->result = g->board[i][k];

				}
				if (g->board[i][k] == g->board[i - 1][k]
						&& g->board[i][k] == g->board[i - 2][k]
						&& g->board[i][k] == g->board[i - 3][k]) {
					g->finish = TRUE;
					g->result = g->board[i][k];
				}
				if (g->board[i][k] == g->board[i - 1][k + 1]
						&& g->board[i][k] == g->board[i - 2][k + 2]
						&& g->board[i][k] == g->board[i - 3][k + 3]) {
					g->finish = TRUE;
					g->result = g->board[i][k];
				}
				if (g->board[i][k] == g->board[i][k - 1]
						&& g->board[i][k] == g->board[i][k - 2]
						&& g->board[i][k] == g->board[i][k - 3]) {
					g->finish = TRUE;
					g->result = g->board[i][k];
				}
				if (g->board[i][k] == g->board[i][k + 1]
						&& g->board[i][k] == g->board[i][k + 2]
						&& g->board[i][k + 3] == g->board[i][k]) {
					g->finish = TRUE;
					g->result = g->board[i][k];
				}
				if (g->board[i][k] == g->board[i + 1][k - 1]
						&& g->board[i][k] == g->board[i + 2][k - 2]
						&& g->board[i][k] == g->board[i + 3][k - 3]) {
					g->finish = TRUE;
					g->result = g->board[i][k];
				}
				if (g->board[i][k] == g->board[i + 1][k]
						&& g->board[i][k] == g->board[i + 2][k]
						&& g->board[i][k] == g->board[i + 3][k]) {
					g->finish = TRUE;
					g->result = g->board[i][k];
				}
				if (g->board[i][k] == g->board[i + 1][k + 1]
						&& g->board[i][k] == g->board[i + 2][k + 2]
						&& g->board[i][k] == g->board[i + 3][k + 3]) {
					g->finish = TRUE;
					g->result = g->board[i][k];
				}
			}
		}
	}
}

