#ifndef GEOMETRY_H_
#define GEOMETRY_H_

#include "../graphic/simple_graphics.h"

typedef struct{
	int left;
	int right;
	int top;
	int bottom;
	int x_center;
	int y_center;
	bool set;
}SQUARE;

typedef struct{
    int left;
    int right;
    int top;
    int bottom;
    SQUARE squares[42];
}RECT;

typedef struct{
    int x;
    int y;
}POINT;

bool IsPtInRect(POINT *pt, RECT *rc);
int FindSquare(POINT *pt,RECT *rc);
void PtSet(POINT *pt, int x, int y);
void RectSet(RECT *rc, int left, int right, int top, int bottom);
void RectInflate(RECT *rc, int x, int y);
void RectOffset(RECT *rc, int x, int y);
void RectCopy(RECT *rcDes, RECT *rcSrc);
int RectWidth(RECT *rc);
int RectHeight(RECT *rc);
int PtDistance(POINT *pt1, POINT *pt2);
void PtCopy(POINT *ptDes, POINT *ptSrc);

bool CheckTriangleBorder(POINT *pt, triangle_struct *tr);
void TriangleSet(int n_col,int y_start,int x_start, triangle_struct *triangle);


#endif /*GEOMETRY_H_*/
